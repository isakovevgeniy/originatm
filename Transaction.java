package patProject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Transaction {
    private int id;
    private Date date;
    private char type;
    public final static char WITH_DRAW = '-';
    public final static char DEPOSIT = '+';
    private double amount;
    private double balance;
    private String descriptions;
    public static ArrayList<Transaction> transactions = new ArrayList<>();

    public Transaction (int id, char type, double amount, double balance, String descriptions) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.balance = balance;
        this.descriptions = descriptions;
        date = new Date();
    }

    public int getId() {
        return id;
    }

    public char getType() {
        return type;
    }

    public double getAmount() {
        return amount;
    }

    public double getBalance() {
        return balance;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public Date getDate() {
        return date;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public static void printInfo() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy / HH:mm:ss");
        System.out.println();
        System.out.format("%22s %3s %7s %10s %14s %n",
                "Дата",
                "ID",
                "Тип транзакции",
                "Сумма, руб",
                "Баланс, руб."
        );
        for (Transaction transaction: Transaction.transactions) {
            System.out.format("%22s %3d %7c %17.2f %13.2f %n",
                    dateFormat.format(transaction.getDate()),
                    transaction.getId(),
                    transaction.getType(),
                    transaction.getAmount(),
                    transaction.getBalance()
            );
        }
    }
}
