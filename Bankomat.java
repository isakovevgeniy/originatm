package patProject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Scanner;

public class Bankomat {
    private int id;
    private boolean IdExist;
    private double summ = 0;

    private final String[] MENU = {
            "   Основное меню",
            "1: проверить баланс счета",
            "2: снять со счета",
            "3: положить на счет",
            "4: выйти",
            "0: завершить работу системы"
    };

    //Выполнение действий согласно меню
    public void action (BufferedReader input) {
        int menuItem;
        while(true) {
            menuItem = 0;

            getId(input);
            greating();
            printMenu();
            while (menuItem != 4) {
                menuItem = getMenuNumber(input);
                switch (menuItem) {
                    case (1):
                        printBalance();
                        break;
                    case (2):
                        withDraw(input);
                        break;
                    case (3):
                        deposit(input);
                        break;
                    case (4):
                        break;
                    case (0):
                        return;
                }
            }
        }
    }

    private void greating() {
        String nameAndPatronymic = AccountTest.accountsList.get(id).getName();
        nameAndPatronymic = nameAndPatronymic.split(" ", 2)[1];
        System.out.printf("Добрый день %s%n", nameAndPatronymic);
    }

    private void printMenu() {
        for (String line : MENU)
            System.out.println(line);
    }

    private void getId(BufferedReader input) {
        int inputNumber;
        do {
            System.out.print("Введите номер ID: ");
            inputNumber = (int) inputToDouble(input);
            isIdExist(inputNumber);
        } while (!IdExist);
        id = inputNumber;
    }

    private void isIdExist(int id) {
        for (Account account : AccountTest.accountsList)
            if (account.getId() == id) {
                IdExist = true;
                return;
            }
        IdExist = false;
        System.out.println("Id с таким номером не существует.");
    }

    private int getMenuNumber(BufferedReader input) {
        System.out.print("Введите пункт меню: ");
        return (int) inputToDouble(input);
    }

    private double inputToDouble(BufferedReader input) {
        double inputNumber;
        do {
            try {
                inputNumber = Double.parseDouble(consoleInput(input));
                break;
            } catch (Exception e) {
                System.out.println("Введено некорректное значение.");
                System.out.print("Повторите ввод: ");
            }
        } while (true);
        return inputNumber;
    }

    private String consoleInputThrowException(BufferedReader input) throws IOException {
        while (true) {
            String inputString = input.readLine();
            if (inputString == null || inputString.equals("")){
                System.out.println("Введена пустая строка.");
                System.out.print("Повторите ввод: ");
            } else return inputString;
        }
    }
    private String consoleInput(BufferedReader input){
        try{
            return consoleInputThrowException(input);
        } catch (IOException e) {
            System.out.println("IOException caught");
            return null;
        }
    }

    //Операция снятия
    private void withDraw(BufferedReader input) {
        System.out.print("Введите сумму снятия: ");
        summ = inputToDouble(input);
        if (AccountTest.accountsList.get(id).getBalance() >= summ) {
            AccountTest.accountsList.get(id).setBalance(AccountTest.accountsList.get(id).getBalance() - summ);
            Transaction.transactions.add(new Transaction(id, Transaction.WITH_DRAW, summ, AccountTest.accountsList.get(id).getBalance(), ""));
            System.out.print("Cнятие выполнено. ");
            printBalance();
        } else System.out.println("Суммы на вашем счету недостаточно");
    }

    //Операция пополнения
    private void deposit(BufferedReader input) {
        System.out.print("Введите сумму пополнения: ");
        summ = inputToDouble(input);
        AccountTest.accountsList.get(id).setBalance(AccountTest.accountsList.get(id).getBalance() + summ);
        Transaction.transactions.add(new Transaction(id, Transaction.DEPOSIT, summ, AccountTest.accountsList.get(id).getBalance(), ""));
        System.out.print("Пополнение выполнено. ");
        printBalance();
    }
    //Вывести баланс на экран
    private void printBalance() {
        System.out.printf("Ваш баланс составляет: %.2f руб.%n", AccountTest.accountsList.get(id).getBalance());
    }
}