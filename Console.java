package patProject;

import java.io.*;

public class Console {
    static void setSystem_In(){
        //Тестируем программу на правильность отработки некорректного ввода
        //кладем данные в строку.
        StringBuilder sb = new StringBuilder();
        sb.append("3").append('\n');        //ID = 3
        sb.append("1").append('\n');        //print balance
        sb.append("2").append('\n');        //with draw
        sb.append("300").append('\n');      //summ = 300
        sb.append("1").append('\n');        //print balance
        sb.append("3").append('\n');        //deposit
        sb.append("3000").append('\n');     //summ = 3000
        sb.append("4").append('\n');        //exit

        sb.append("text text").append('\n');    //ID = text text
        sb.append('\n');                        //ID = null
        sb.append(".02").append('\n');          //ID = .02
        sb.append("18").append('\n');           //ID = 18

        sb.append("5").append('\n');            //ID = 5
        sb.append("1").append('\n');            //print balance

        sb.append("345").append('\n');          //menu =345
        sb.append("text text").append('\n');    //ID =text text

        sb.append("2").append('\n');            //with draw

        sb.append("text text").append('\n');    //text text
        sb.append('\n');                        //ID = null

        sb.append("800").append('\n');      //summ = 800
        sb.append("1").append('\n');        //print balance
        sb.append("3").append('\n');        //deposit
        sb.append("15980").append('\n');    //summ = 15980

        sb.append("0").append('\n');        //shutdown

        String data = sb.toString();

        //Оборачиваем строку в класс ByteArrayInputStream
        InputStream is = new ByteArrayInputStream(data.getBytes());
        System.setIn(is);
    }
}