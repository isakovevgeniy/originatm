package patProject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class AccountTest {
    static String[] namesArray = {
            "Иванов Иван Иванович",
            "Петров Петр Петрович",
            "Сидоров Сидор Сидорович",
            "Павлов Павел Павлович",
            "Захаров Захар Захарович",
            "Никитин Никита Никитьевич",
            "Константинов Константин Константинович"};

    static ArrayList<Account> accountsList = new ArrayList<>();
    static {
        //Установили процентную ставку
        Account.setAnnualInterestRate(5.5);

        //Создали 7 аккаунтов
        for (int i = 0; i < 7; i++ ) {
            accountsList.add(new Account(i, namesArray[i],10000));
        }
    }

    public static void main(String[] args) {
        //Имитацияя ввода
        Console.setSystem_In();

        //Запустили работу банкомата
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        new Bankomat().action(input);
        try {
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //по окончании работвы АТМ. вывод списка произведенных транзакций
        Transaction.printInfo();
    }
}
