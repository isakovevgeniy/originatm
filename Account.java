package patProject;

import java.util.Date;

public class Account {
    private static double annualInterestRate;
    private int id;
    private double balance;
    private Date dateCreate;
    private String name; // должен быть массив {Ф,И,О}

    public Account () {
        this.id = 0;
        this.balance = 0;
        this.dateCreate = new Date();
    }

    public Account (int id, String name, double balance) {
        this.name = name;
        this.id = id;
        this.balance = balance;
        this.dateCreate = new Date();
    }

    public int getId() {
        return id;
    }

    public double getBalance() {
        return this.balance;
    }

    public String getName() { return name; }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Date getDateCreate() {
        return this.dateCreate;
    }

    public static double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public static void setAnnualInterestRate(double annualInterestRate) {
        Account.annualInterestRate = annualInterestRate;
    }
    public static void printInfo(Account account) {
        System.out.printf("Id: %d. Баланс счета: %.2f. Ежемесячные проценты: %.2f. Дата создания счета: %4$td %4$tb %4$tY%n",
                account.getId(), account.getBalance(), account.getMonthlyInterest(), account.getDateCreate());
    }

    public double getMonthlyInterest() {
        return getBalance() * (Account.annualInterestRate/100)/12;
    }
}
